import hashlib
from multiprocessing import Pool
from bitstring import BitArray, Bits


HASH_LEN = 128


def parse_input():
    num = int(input())

    texts = []
    queries = []
    for _ in range(num):
        line = input()
        texts.append(line)

    k = int(input())
    for _ in range(k):
        nums = input()
        query = tuple(map(int, nums.split()))
        queries.append(query)

    return texts, queries


def all_hammings_once(current, hashes, bound):
    mine = hashes[current]
    counter = -1
    for theirs in hashes:
        distance = hamming(mine, theirs)
        if distance <= bound:
            counter += 1
    return counter


def simhash(text):
    sh = [0] * HASH_LEN
    bit_res = BitArray(length=HASH_LEN)
    splits = text.split()
    for split in splits:
        handle_split(split, sh)

    for i in range(HASH_LEN):
        if sh[i] >= 0:
            bit_res[i] = True

    # assert len(res_num.hex) == 32
    return bit_res


def handle_split(split, sh):
    md = hashlib.new('md5')
    md.update(split.encode())
    digest = md.hexdigest()
    bits = Bits(hex=digest)
    # print(bits)

    for i in range(HASH_LEN):
        sh[i] = (sh[i] + 1) if bits[i] else (sh[i] - 1)


def hamming(x, y):
    assert len(x) == len(y)
    distance = (x ^ y).count(1)
    # distance = sum(c1 != c2 for c1, c2 in zip(x, y))
    # print("{} {} {}".format(b1, b2, distance))
    return distance


texts, queries = parse_input()
hashes = []
with Pool(16) as p:
    result = p.map(simhash, texts)
    hashes = result


def process_query(query):
    ind, bnd = query
    return all_hammings_once(ind, hashes, bnd)


with Pool(16) as p:
    result = p.map(process_query, queries)
    for r in result:
        print(r)
