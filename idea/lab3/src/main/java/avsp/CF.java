package avsp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;

public class CF {

    public static void main(String[] args) {
        handleQueries();
    }

    private static void handleQueries() {
        InputStreamReader in = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(in);
        try {
            double itemItemMatrix[][] = createItemItemMatrix(br);
            double normalizedItemItem[][] = copyMatrix(itemItemMatrix);
            normalizeMatrix(normalizedItemItem);

            double userUserMatrix[][] = copyMatrix(itemItemMatrix);
            double copiedUserMatrix[][] = copyMatrix(userUserMatrix);
            userUserMatrix = transposeMatrix(copiedUserMatrix);
            double normalizedUserUser[][] = transposeMatrix(copiedUserMatrix);
            normalizeMatrix(normalizedUserUser);

            //printMatrix(normalizedItemItem);
            //printMatrix(normalizedUserUser);

            int numQueries = Integer.parseInt(br.readLine());
            for (int q = 0; q < numQueries; q++) {
                String parts[] = br.readLine().split(" ");
                int itemInd = Integer.parseInt(parts[0]) - 1;
                int userInd = Integer.parseInt(parts[1]) - 1;
                boolean transposed = parts[2].equals("1");
                int maxCardinality = Integer.parseInt(parts[3]);

                double startingMatrix[][] = null;
                double normalizedMatrix[][] = null;

                //System.out.println("Similarity for item: " + itemInd + " and user " + userInd);
                if (!transposed) {
                    startingMatrix = itemItemMatrix;
                    normalizedMatrix = normalizedItemItem;
                } else {
                    int temp = itemInd;
                    itemInd = userInd;
                    userInd = temp;
                    startingMatrix = userUserMatrix;
                    normalizedMatrix = normalizedUserUser;
                }
                ArrayList<SimilarityValue> candidates = calculateSimilarities(normalizedMatrix, startingMatrix, itemInd, userInd, maxCardinality);

                //System.out.println("Similarity for item: " + itemInd + " and user " + userInd);
                double numerator = 0.0;
                double denominator = 0.0;
                for (SimilarityValue cand: candidates) {
                    if (cand.value == 0.0) {
                        continue;
                    }
                    //System.out.println(cand.similarity + " | " + cand.value);
                    numerator += cand.similarity * cand.value;
                    denominator += cand.similarity;
                }

                double finalRating = numerator / denominator;
                DecimalFormat df = new DecimalFormat("#.000");
                BigDecimal bd = new BigDecimal(finalRating);
                BigDecimal res = bd.setScale(3, RoundingMode.HALF_UP);
                System.out.println(df.format(res));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void normalizeMatrix(double[][] matrix) {
        int rows = matrix.length;
        int cols = matrix[0].length;

        for (int row = 0; row < rows; row++) {
            double rowSum = 0.0;
            int posCount = 0;
            for (int col = 0; col < cols; col++) {
                rowSum += matrix[row][col];
                if (matrix[row][col] != 0.0) {
                    posCount += 1;
                }
            }
            double avg = rowSum / ((double) posCount);

            for (int col = 0; col < cols; col++) {
                if (matrix[row][col] != 0.0) {
                    matrix[row][col] -= avg;
                }
            }
        }
    }

    private static double[][] copyMatrix(double[][] matrix) {
        int rows = matrix.length;
        int cols = matrix[0].length;

        double clone[][] = new double[rows][cols];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                clone[row][col] = matrix[row][col];
            }
        }
        return clone;
    }

    private static double[][] transposeMatrix(double[][] matrix) {
        int rows = matrix.length;
        int cols = matrix[0].length;

        double clone[][] = new double[cols][rows];
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                clone[col][row] = matrix[row][col];
            }
        }
        return clone;
    }

    private static void printMatrix(double[][] matrix) {
        int rows = matrix.length;
        int cols = matrix[0].length;

        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static ArrayList<SimilarityValue> calculateSimilarities(double[][] normalizedMatrix, double[][] startingMatrix, int itemInd, int userInd, int maxCardinality) {
        int rows = normalizedMatrix.length;
        int cols = normalizedMatrix[0].length;

        //System.out.printf("Query indices: row = %d, col = %d\n", itemInd + 1, userInd + 1);
        ArrayList<SimilarityValue> candidates = new ArrayList<SimilarityValue>();
        double similarities[] = new double[cols];
        for (int item = 0; item < rows; item++) {
            double numerator = 0.0;
            double first = 0.0;
            double second = 0.0;

            for (int user = 0; user < cols; user++) {
                numerator += normalizedMatrix[item][user] * normalizedMatrix[itemInd][user];
                first += normalizedMatrix[item][user] * normalizedMatrix[item][user];
                second += normalizedMatrix[itemInd][user] * normalizedMatrix[itemInd][user];
            }

            double similarity = numerator / Math.sqrt(first * second);
            similarities[item] = similarity;
            if (similarity > 0.0 && item != itemInd && startingMatrix[item][userInd] > 0.0) {
                //System.out.printf("Candidate indices: row = %d, col = %d (value: %.1f) (similarity: %.3f)\n", item, userInd, startingMatrix[item][userInd], similarity);
                SimilarityValue indexSimilarity = new SimilarityValue(similarity, startingMatrix[item][userInd]);
                candidates.add(indexSimilarity);
                Collections.sort(candidates);
                if (candidates.size() > maxCardinality) {
                    candidates.remove(0);
                }
            }
        }
        //for (double sim: similarities) {
        //    System.out.println(sim);
        //}
        return candidates;
    }

    private static double[][] createItemItemMatrix(BufferedReader br) throws IOException {
        String str = br.readLine();
        String pcs[] = str.split(" ");
        int rows = Integer.parseInt(pcs[0]);   //broj itema
        int cols = Integer.parseInt(pcs[1]);   //broj usera

        double matrix[][] = new double[rows][];
        for (int row = 0; row < rows; row++) {
            str = br.readLine();
            pcs = str.split(" ");

            double users[] = new double[cols];
            for (int col = 0; col < cols; col++) {
                if (pcs[col].equals("X")) {
                    users[col] = 0.0;
                } else {
                    users[col] = Double.parseDouble(pcs[col]);
                }
            }
            matrix[row] = users;
        }
        return matrix;
    }

    static class SimilarityValue implements Comparable<SimilarityValue> {
        double similarity;
        double value;

        SimilarityValue(double sim, double value) {
            this.similarity = sim;
            this.value = value;
        }

        @Override
        public int compareTo(SimilarityValue other) {
            return ((Double) similarity).compareTo(other.similarity);
        }
    }
}
