import numpy as np

from numpy import dot
from numpy.linalg import norm

def calc_jaccard(x, y):
    mins = 0
    maxs = 0
    for i in range(len(x)):
        mins += min(x[i], y[i])
        maxs += max(x[i], y[i])
    return mins/maxs

a = [0, 0, 5, 1]
b = [0, 4, 4, 1]

cos_sim = dot(a, b)/(norm(a)*norm(b))
print(f"Cosine similarity: {cos_sim}")
print(f"Cosine distance: {1 - cos_sim}\n")

jaccard = calc_jaccard(a, b)
print(f"Jaccard similarity: {jaccard}")
print(f"Jaccard distance: {1 - jaccard}\n")
